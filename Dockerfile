FROM mongo:latest
ENV MONGO_INITDB_ROOT_USERNAME=root
ENV MONGO_INITDB_ROOT_PASSWORD=1209@root
ENV MONGO_USERNAME=nielsendb
ENV MONGO_PASSWORD=nielsendb
ENV MONGO_INITDB_DATABASE=nielsen
# expose MongoDB's default port of 27017
ADD mongo-init.js /docker-entrypoint-initdb.d/
EXPOSE 27017
